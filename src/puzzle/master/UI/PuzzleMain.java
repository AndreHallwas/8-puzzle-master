/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puzzle.master.UI;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import puzzle.master.UI.casos.Cases;
import puzzle.master.UI.celula.Celulas;
import puzzle.master.UI.threads.Threads;
import puzzle.master.UI.webcam.WebCam;
import puzzle.master.engine.AEstrela;
import puzzle.master.engine.Busca;
import puzzle.master.engine.Largura;
import puzzle.master.engine.Profundidade;
import puzzle.master.engine.node.node;
import puzzle.util.Util;

/**
 *
 * @author Aluno
 */
public class PuzzleMain implements Initializable {

    private static boolean Blocked;
    private WebCam wc;
    @FXML
    private GridPane grid;
    @FXML
    private Menu menuAlgoritmos;
    private Busca b;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Start();
    }

    public void Start() {
        try {
            wc = new WebCam();
            Celulas.constroiPosicao(grid.getChildren());
            Celulas.addImagems(getImagem());
            Celulas.link();
            setDefaultAlgorithm();
        } catch (Exception ex) {
            System.out.println(ex.getCause());
        }
    }

    public Image[][] getImagem() {
        Image img = null;
        Image[][] im = null;
        try {
            img = new Image(this.getClass().getResourceAsStream("/Imagens/default.jpg"));
        } catch (Exception ex) {
            System.out.println(ex.getCause());
        }
        if (img == null) {
            img = SwingFXUtils.toFXImage(new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB), null);
        }
        im = Util.processa_ImagemFaster(img);
        //im[2][2] = null;
        return im;
    }

    @FXML
    private void evtEmbaralhar(MouseEvent event) {
        if (!Blocked) {
            Celulas.embaralhar();
        }
    }

    @FXML
    private void evtCaso1(Event event) {
        Celulas.change(Cases.getCaso1().getEstado());
    }

    @FXML
    private void evtCaso2(Event event) {
        Celulas.change(Cases.getCaso2().getEstado());
    }

    @FXML
    private void evtCaso3(Event event) {
        Celulas.change(Cases.getCaso3().getEstado());
    }

    @FXML
    private void evtBuscaSolucao(MouseEvent event) {
        if (!Blocked) {
            Thread tm = new Thread(()
                    -> {
                Thread t = new Thread(()
                        -> {
                    synchronized (this) {
                        if (b != null) {
                            node Solucao = Cases.getSolucao();
                            node Problema = new node(Celulas.convertArraytoMatrix3(Celulas.getEstadoAtual())) {
                            };

                            b.run(Solucao, Problema);
                            if (b.getResolvido()) {
                                Celulas.cinematicResolution(b.getCaminhoSolucao());
                            }
                        }
                        notify();
                    }
                });
                t.start();
                Threads.setOffBusca(t);
                synchronized (t) {
                    try {
                        System.out.println("Aguardando o b completar...");
                        PuzzleMain.setBlocked(true);
                        t.wait();
                    } catch (InterruptedException e) {
                        System.out.println(e);
                    } finally {
                        PuzzleMain.setBlocked(false);
                    }

                    System.out.println("Ok!!!!!");
                }
            });
            tm.start();
            Threads.setMainBusca(tm);
        }
    }

    @FXML
    private void evtRestaurarGrid(MouseEvent event) {
        if (!Blocked) {
            Celulas.change(Cases.getDefault());
        }
    }

    private void moverImagem(int x, int y) {
        if (!Blocked) {
            Celulas.moverImagem(x, y);
        }
    }

    @FXML
    private void clkIV0(MouseEvent event) {
        moverImagem(0, 0);
    }

    @FXML
    private void clkIV1(MouseEvent event) {
        moverImagem(0, 1);
    }

    @FXML
    private void clkIV2(MouseEvent event) {
        moverImagem(0, 2);
    }

    @FXML
    private void clkIV3(MouseEvent event) {
        moverImagem(1, 0);
    }

    @FXML
    private void clkIV4(MouseEvent event) {
        moverImagem(1, 1);
    }

    @FXML
    private void clkIV5(MouseEvent event) {
        moverImagem(1, 2);
    }

    @FXML
    private void clkIV6(MouseEvent event) {
        moverImagem(2, 0);
    }

    @FXML
    private void clkIV7(MouseEvent event) {
        moverImagem(2, 1);
    }

    @FXML
    private void clkIV8(MouseEvent event) {
        moverImagem(2, 2);
    }

    @FXML
    private void evtAbrir(Event event) {
        FileChooser fileChooser = new FileChooser();

        FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
        FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        fileChooser.getExtensionFilters().addAll(extFilterJPG, extFilterPNG);

        File file = fileChooser.showOpenDialog(null);
        if (file != null) {
            try {

                BufferedImage bufferedImage = ImageIO.read(file);

                int factor = Util.getFactor(bufferedImage);

                Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                Image[][] im = Util.processa_Imagem(Util.scale(image, factor, factor, false));

                Celulas.clearImagems();
                Celulas.addImagems(im);
                Celulas.link();

            } catch (IOException ex) {
                System.out.println(ex);
            }
        }
    }

    @FXML
    private void evtInformação(Event event) {
        try {
            Stage stage = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("Relatorio.fxml"));

            Scene scene = new Scene(root);
            scene.getStylesheets().add(this.getClass().getResource("css/Estilo.css").toExternalForm());

            stage.setScene(scene);
            stage.getIcons().add(new Image(this.getClass().getResourceAsStream("/Imagens/icone.png")));
            stage.setMaximized(false);
            stage.setResizable(false);
            stage.setTitle("Relatório");
            stage.show();
            /*
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText("Em Desemvolvimento.");
            alert.setTitle("Em Desemvolvimento");
            alert.showAndWait();*/
        } catch (IOException ex) {
            Logger.getLogger(PuzzleMain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void setProfundidade(Event event) {
        if(!Blocked)
        {    
        b = new Profundidade();
        clearMenuAlgoritmosExcept("Profundidade");
        }
    }

    @FXML
    private void setLargura(Event event) {
        if(!Blocked)
        {
        b = new Largura();
        clearMenuAlgoritmosExcept("Largura");
        }
    }

    @FXML
    private void setAEstrela(Event event) {
        if(!Blocked)
        {    
        b = new AEstrela();
        clearMenuAlgoritmosExcept("A*");
        }
    }

    private void setDefaultAlgorithm() {
        if(!Blocked)
        {
        b = new Largura();
        clearMenuAlgoritmosExcept("Largura");
        }
    }

    private void clearMenuAlgoritmosExcept(String Nome) {
        RadioMenuItem itemR = null;
        for (MenuItem item : menuAlgoritmos.getItems()) {
            if (item instanceof RadioMenuItem) {
                itemR = (RadioMenuItem) item;
                if (item.getText().contains(Nome)) {
                    itemR.setSelected(true);
                } else {
                    itemR.setSelected(false);
                }
            }
        }
    }

    @FXML
    private void evtStartModoWebCam(MouseEvent event) {
        try {
            if (wc.getWebcam() == null || !wc.getWebcam().isOpen()) {
                wc.init();
            } else {
                wc.cancel();
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    private static void setBlocked(boolean Blocked) {
        PuzzleMain.Blocked = Blocked;
    }

}
