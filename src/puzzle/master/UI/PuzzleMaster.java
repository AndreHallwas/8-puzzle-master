/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puzzle.master.UI;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import puzzle.master.UI.threads.Threads;

/**
 *
 * @author Aluno
 */
public class PuzzleMaster extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        
        stage.setOnCloseRequest((WindowEvent e) -> {
            Threads.Finalizar();
            Platform.exit();
            System.exit(0);
        });
        
        Parent root = FXMLLoader.load(getClass().getResource("PuzzleMain.fxml"));

        Scene scene = new Scene(root);
        scene.getStylesheets().add(this.getClass().getResource("css/Estilo.css").toExternalForm());

        stage.setScene(scene);
        stage.getIcons().add(new Image(this.getClass().getResourceAsStream("/Imagens/icone.png")));
        stage.setMaximized(false);
        stage.setResizable(false);
        stage.setTitle("8 Puzzle Master");
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
