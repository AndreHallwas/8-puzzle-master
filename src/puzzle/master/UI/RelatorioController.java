/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puzzle.master.UI;

import Relatorio.RelatorioClass;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXRadioButton;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Timer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import puzzle.master.UI.casos.Cases;
import puzzle.master.engine.AEstrela;
import puzzle.master.engine.Busca;
import puzzle.master.engine.Profundidade;
import puzzle.master.engine.node.node;

/**
 * FXML Controller class
 *
 * @author luis
 */
public class RelatorioController implements Initializable
{
    private ToggleGroup group;
    private final List<node>Casos = new ArrayList<>();
    @FXML
    private JFXRadioButton rbni;
    @FXML
    private JFXRadioButton rbh;
    @FXML
    private Label lblNome;
    @FXML
    private Label lblDesc;
    @FXML
    private TableView<Object> tabela;//Busca
    @FXML
    private TableColumn<Object, Object> colCaso;
    @FXML
    private TableColumn<Object, Object> colEstadoIni;
    @FXML
    private TableColumn<Object, Object> colEstadoFinal;
    @FXML
    private TableColumn<Object, Object> colTempo;
    @FXML
    private TableColumn<Object, Object> colCountPassos;
    @FXML
    private JFXCheckBox chTipoResultados;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        try
        {
            initComponents();
        }
        catch(Exception ex)
        {
            System.out.println(ex.getCause()+"\n"+ex.getMessage());
        }
        
    }

    private void initComponents()
    {
        colCaso.setCellValueFactory(new PropertyValueFactory("caso"));
        colCountPassos.setCellValueFactory(new PropertyValueFactory("ProfundFinal"));
        colEstadoFinal.setCellValueFactory(new PropertyValueFactory("solucao"));// ou solução
        colEstadoIni.setCellValueFactory(new PropertyValueFactory("problema"));
        colTempo.setCellValueFactory(new PropertyValueFactory("tempo"));
        //iniciaColunas(true);
        
        group = new ToggleGroup();
        rbh.setToggleGroup(group);
        rbni.setToggleGroup(group);
        
        Casos.add(Cases.getCaso1());//caso 1
        Casos.add(Cases.getCaso2());//caso 2
        Casos.add(Cases.getCaso3());//caso 3
        
        atualizaInfo();
    }

    private void processaResultados(boolean FLAG)
    {
        Busca b;
        ObservableList<Object> ob = FXCollections.observableArrayList();
        tabela.getItems().clear();
        for (int i = 0; i < Casos.size(); i++)
        {
            try
            {
                b = (FLAG)? new Profundidade(i+1) : new AEstrela(i+1);
                b.run(Cases.getSolucao(), Casos.get(i));
                ob.add(b);
                
            }
            catch(Exception ex)
            {
                System.out.println(ex.getMessage());
            }
            
        }
        tabela.setItems(ob);
        
    }

    @FXML
    private void evtGerarResultado(MouseEvent event)
    {
        if(chTipoResultados.isSelected())
            processaArquivo(rbni.isSelected());
        else
            processaResultados(rbni.isSelected());
    }

    @FXML
    private void clkNaoInformada(MouseEvent event)
    {
        atualizaInfo();
    }

    @FXML
    private void clkHeuristica(MouseEvent event)
    {
        atualizaInfo();
    }

    private void atualizaInfo()
    {
        tabela.getItems().clear();
        if(rbni.selectedProperty().getValue())
        {
            lblNome.setText("Busca em Profundidade");
            lblDesc.setText("Algoritmo usado para realizar uma busca ou travessia numa árvore, estrutura de árvore ou grafo. Intuitivamente, \n"
                    + "o algoritmo começa num nó raiz (selecionando algum nó como sendo o raiz, no caso de um grafo) e explora tanto \n"
                    + "quanto possível cada um dos seus ramos, antes de retroceder(backtracking)");
        }
        else if(rbh.selectedProperty().getValue())
        {
            lblNome.setText("Busca A*(Manhattan Distance)");
            lblDesc.setText("Algoritmo para Busca de Caminho. Ele busca o caminho em um grafo de um vértice inicial até um vértice final. \n"
                    + "Ele é a combinação de aproximações heurísticas como do algoritmo Breadth First Search (Busca em Largura) e da \n"
                    + "formalidade do Algoritmo de Dijkstra.");
        }
    }
    private void processaArquivo(Boolean value)
    {
        ObservableList<Object> lr = FXCollections.observableArrayList(RelatorioClass.getRelatorioPreGerado(value,this));
        tabela.setItems(lr);
    }
    
}
