/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puzzle.master.UI.celula;

/**
 *
 * @author Aluno
 */
public class Seq {

    private int[] seq;

    public Seq() {
        seq = new int[9];
        for (int i = 0; i < seq.length; i++) {
            seq[i] = 0;
        }
    }

    public int next() {
        boolean flag = true;
        int Pos = 0;
        while (Pos < seq.length && flag) {
            if (seq[Pos] == 0) {
                seq[Pos] = 1;
                flag = false;
            }
            Pos++;
        }
        return Pos;
    }

    public void remove(int pos) {
        seq[pos] = 0;
    }
}
