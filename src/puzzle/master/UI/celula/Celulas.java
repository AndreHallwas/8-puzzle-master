/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puzzle.master.UI.celula;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javafx.animation.KeyFrame;
import javafx.animation.SequentialTransition;
import javafx.animation.Timeline;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
import puzzle.master.algorithms.Fila;
import puzzle.master.engine.Busca;

/**
 *
 * @author Aluno
 */
public class Celulas {

    private volatile static ArrayList<celula> lista;
    private volatile static ArrayList<posicao> posicoes;

    static {
        Celulas.posicoes = new ArrayList();
        Celulas.lista = new ArrayList();
    }

    public static void constroiPosicao(ObservableList<Node> pn) {
        ObservableList<Node> componentes = pn;
        ImageView imgV = null;
        Label lblPos = null;
        for (Node n : componentes) {
            if (n instanceof Pane) {
                constroiPosicao(((Pane) n).getChildren());
            }
            if (n instanceof ImageView) {
                imgV = (ImageView) n;
            }
            if (n instanceof Label) {
                lblPos = (Label) n;
            }
        }
        if (imgV != null && lblPos != null) {
            addPosicao(imgV, lblPos);
        }
    }

    public static void addPosicao(ImageView imgV, Label lblPos) {
        posicoes.add(new posicao(imgV, lblPos));
    }

    public static void clearPosicoes() {
        posicoes.clear();
    }

    public static void addImagem(Image imagem, int Pos) {
        getLista().add(new celula(imagem, Pos));
    }

    public static void setImagem(Image imagem, int Pos) {
        getLista().get(Pos-1).setImg(imagem);
    }

    public static void clearImagems() {
        getLista().clear();
    }

    public static void link() {
        for (celula li : getLista()) {
            int Pos = 0;
            while (Pos < posicoes.size() && posicoes.get(Pos).getPos() != li.getPos()) {
                Pos++;
            }
            if (Pos < posicoes.size()) {
                posicoes.get(Pos).getImageView().setImage(null);
                posicoes.get(Pos).getImageView().setImage(li.getImg());
                posicoes.get(Pos).setLblPos(Pos+1);
            }
        }
    }

    public static void change(int[][] estado) {
        List<Integer> estadoAtual = convertMatrizToList(estado);
        celula celulaAtual;
        posicao posicaoAtual;
        int i = 0;
        while (i < estadoAtual.size()) {
            posicaoAtual = posicoes.get(i);
            celulaAtual = getLista().get(estadoAtual.get(i)-1);
            posicaoAtual.getImageView().setImage(celulaAtual.getImg());
            celulaAtual.setPos(estadoAtual.get(estadoAtual.get(i)-1));
            posicaoAtual.setLblPos(estadoAtual.get(i));
            i++;
        }
    }

    public static void clearLink() {
        for (posicao pos : posicoes) {
            pos.getImageView().setImage(null);
        }
    }

    public static List<Integer> convertMatrizToList(int[][] values) {
        List<Integer> array = new ArrayList();
        for (int[] value : values) {
            for (int j = 0; j < value.length; j++) {
                array.add(value[j]);
            }
        }
        return array;
    }

    public static void swapImagem(int i, int k) {
        Image temp = getLista().get(i).getImg();
        int tempint = getLista().get(i).getPos();

        getLista().get(i).setImg(getLista().get(k).getImg());
        getLista().get(i).setPos(getLista().get(k).getPos());

        getLista().get(k).setImg(temp);
        getLista().get(k).setPos(tempint);

    }

    public static int[] getEstadoAtual() {
        int[] estado = new int[getLista().size()];
        for (int i = 0; i < estado.length; i++) {
            estado[i] = getLista().get(i).getPos();
        }
        return estado;
    }

    public static void embaralhar() {
        Random rand = new Random();
        change(embaralhar(convertArraytoMatrix3(getEstadoAtual()), rand.nextInt(50) + 5/*Min = 5*/, rand));
    }

    private static int[][] embaralhar(int[][] estado, int Quant, Random rand) {
        int Pos;
        if (Quant > 0) {
            int[] posVazio = Busca.buscaEspacoVazio(estado);
            int PosI = posVazio[0], PosJ = posVazio[1];

            int Max = 30;
            boolean flagM = true;
            while (flagM && Max > 0) {
                Pos = rand.nextInt(4);
                if (Pos == 0 && PosI > 0) {
                    estado = Busca.mover(estado, PosI, PosJ, PosI - 1, PosJ);   // move o branco para cima
                } else if (Pos == 1 && PosI < 2) {
                    estado = Busca.mover(estado, PosI, PosJ, PosI + 1, PosJ);   // move o branco para baixo
                } else if (Pos == 2 && PosJ > 0) {
                    estado = Busca.mover(estado, PosI, PosJ, PosI, PosJ - 1);   // move o branco para a esquerda
                } else if (Pos == 3 && PosJ < 2) {
                    estado = Busca.mover(estado, PosI, PosJ, PosI, PosJ + 1);   // move o branco para a direita
                } else {
                    flagM = false;
                }
                Max--;
            }

            return embaralhar(estado, Quant - 1, rand);
        } else {
            return estado;
        }
    }

    public static void moverImagem(String direcao) {
        change(moverImagem(convertArraytoMatrix3(getEstadoAtual()), direcao));
    }

    protected static int[][] moverImagem(int[][] estado, String direcao) {
        int[] posVazio = Busca.buscaEspacoVazio(estado);
        int PosI = posVazio[0], PosJ = posVazio[1];

        if (direcao.equalsIgnoreCase("cima") && PosI > 0) {
            estado = Busca.mover(estado, PosI, PosJ, PosI - 1, PosJ);   // move o branco para cima
        } else if (direcao.equalsIgnoreCase("baixo") && PosI < 2) {
            estado = Busca.mover(estado, PosI, PosJ, PosI + 1, PosJ);   // move o branco para baixo
        } else if (direcao.equalsIgnoreCase("esquerda") && PosJ > 0) {
            estado = Busca.mover(estado, PosI, PosJ, PosI, PosJ - 1);   // move o branco para a esquerda
        } else if (direcao.equalsIgnoreCase("direita") && PosJ < 2) {
            estado = Busca.mover(estado, PosI, PosJ, PosI, PosJ + 1);   // move o branco para a direita
        }

        return estado;
    }

    public static void moverImagem(int PosIN, int PosJN) {
        change(moverImagem(convertArraytoMatrix3(getEstadoAtual()), PosIN, PosJN));
    }

    protected static int[][] moverImagem(int[][] estado, int PosIN, int PosJN) {
        int[] posVazio = Busca.buscaEspacoVazio(estado);
        int PosI = posVazio[0], PosJ = posVazio[1];

        if (PosIN == PosI - 1 && PosI > 0) {
            estado = Busca.mover(estado, PosI, PosJ, PosI - 1, PosJ);   // move o branco para cima
        } else if (PosIN == PosI + 1 && PosI < 2) {
            estado = Busca.mover(estado, PosI, PosJ, PosI + 1, PosJ);   // move o branco para baixo
        } else if (PosJN == PosJ - 1 && PosJ > 0) {
            estado = Busca.mover(estado, PosI, PosJ, PosI, PosJ - 1);   // move o branco para a esquerda
        } else if (PosJN == PosJ + 1 && PosJ < 2) {
            estado = Busca.mover(estado, PosI, PosJ, PosI, PosJ + 1);   // move o branco para a direita
        }

        return estado;
    }

    public static int[][] convertArraytoMatrix3(int[] estadoAtual) {
        int TL = estadoAtual.length / 3;
        int[][] estado = new int[3][];
        for (int i = 0, k = 0; i < estado.length; i++) {
            estado[i] = new int[3];
            for (int j = 0; j < estado[i].length; j++) {
                estado[i][j] = estadoAtual[k++];
            }
        }
        return estado;
    }

    public static void cinematicResolution(Fila resolvido) {
        List<Timeline> timelines = new ArrayList();
        while (!resolvido.isEmpty()) {
            int[][] estado = resolvido.pop().getEstado();
            KeyFrame colorButton = new KeyFrame(Duration.millis(520),
                    (kfActionEvent)
                    -> {
                change(estado);
            });
            Timeline showColor = new Timeline();//create the timeline
            showColor.getKeyFrames().addAll(colorButton);//add the frame to the Timeline
            timelines.add(showColor);
        }
        //Runs the List of Timeline sequentially
        SequentialTransition sequentialTransition = new SequentialTransition();
        sequentialTransition.getChildren().addAll(timelines);//Add all the Timelines created to a SequentialTransition
        sequentialTransition.play();
        sequentialTransition.setOnFinished(stActionEvent
                -> {
            timelines.clear();//Once done, clear the animations
        }
        );
    }

    public static void addImagems(Image[][] imagem) {
        int count = 1;
        if (!lista.isEmpty()) {
            for (Image[] images : imagem) {
                for (Image image : images) {
                    setImagem(image, count++);
                }
            }
        } else {
            for (Image[] images : imagem) {
                for (Image image : images) {
                    addImagem(image, count++);
                }
            }
        }
    }

    /**
     * @return the lista
     */
    public static ArrayList<celula> getLista() {
        return lista;
    }

    public static celula getCelulaVazia() {
        int i = 0;
        while (i < lista.size() && lista.get(i).getPos()== 9) {
            i++;
        }
        return (i < lista.size())?lista.get(i):lista.get(lista.size()-1);
    }

}
