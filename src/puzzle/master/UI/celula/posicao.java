/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puzzle.master.UI.celula;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;

/**
 *
 * @author Aluno
 */
public class posicao
{

    private static final Seq sequencia = new Seq();
    private final int pos = sequencia.next();
    private ImageView Imagem;
    private Label lblPos;

    public posicao(ImageView Imagem, Label lblPos)
    {
        this.Imagem = Imagem;
        this.lblPos = lblPos;
    }

    /**
     * @return the Imagem
     */
    public ImageView getImageView()
    {
        return Imagem;
    }

    /**
     * @param Imagem the Imagem to set
     */
    public void setImageView(ImageView Imagem)
    {
        this.Imagem = Imagem;
    }

    /**
     * @return the pos
     */
    public int getPos()
    {
        return pos;
    }

    /**
     * @param lblPos the lblPos to set
     */
    public void setLblPos(Label lblPos)
    {
        this.lblPos = lblPos;
    }

    public void setLblPos(int Pos)
    {
        Platform.runLater(() ->
        {
            if (lblPos != null)
            {
                lblPos.setText(Pos + "");
            }
        });
    }

}
