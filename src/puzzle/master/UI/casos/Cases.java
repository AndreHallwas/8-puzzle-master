/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puzzle.master.UI.casos;

import puzzle.master.engine.node.node;

/**
 *
 * @author Aluno
 */
public class Cases {

    private static int[][] Default = new int[][]{{1, 2, 3},{4, 5, 6},{7, 8, 9}
    };
    private static node Caso1 = new node(new int[][]{{7, 1, 3}, {2, 6, 9}, {5, 4, 8}}) {
    };//caso 1
    private static node Caso2 = new node(new int[][]{{4, 2, 3}, {6, 9, 1}, {7, 5, 8}}) {
    };//caso 2
    /*private static node Caso3 = new node(new int[][]{{2, 3, 7}, {5, 4, 8}, {9, 6, 1}}) {
    };//caso 3*/
    private static node Caso3 = new node(new int[][]{{2, 3, 5}, {1, 4, 9}, {7, 8, 6}}) {
    };//caso 3
    private static node Solucao = new node(new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}) {
    };

    /**
     * @return the Default
     */
    public static int[][] getDefault() {
        return Default;
    }

    /**
     * @param aDefault the Default to set
     */
    public static void setDefault(int[][] aDefault) {
        Default = aDefault;
    }

    /**
     * @return the Caso1
     */
    public static node getCaso1() {
        return Caso1;
    }

    /**
     * @param aCaso1 the Caso1 to set
     */
    public static void setCaso1(node aCaso1) {
        Caso1 = aCaso1;
    }

    /**
     * @return the Caso2
     */
    public static node getCaso2() {
        return Caso2;
    }

    /**
     * @param aCaso2 the Caso2 to set
     */
    public static void setCaso2(node aCaso2) {
        Caso2 = aCaso2;
    }

    /**
     * @return the Caso3
     */
    public static node getCaso3() {
        return Caso3;
    }

    /**
     * @param aCaso3 the Caso3 to set
     */
    public static void setCaso3(node aCaso3) {
        Caso3 = aCaso3;
    }

    /**
     * @return the Solucao
     */
    public static node getSolucao() {
        return Solucao;
    }

    /**
     * @param aSolucao the Solucao to set
     */
    public static void setSolucao(node aSolucao) {
        Solucao = aSolucao;
    }
    
}
