/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puzzle.master.UI.threads;

import com.github.sarxos.webcam.Webcam;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Remote
 */
public class Threads {

    private static Timer timerWebCam;
    private static TimerTask timerTaskWebCam;
    private static Thread mainBusca;
    private static Thread offBusca;
    private static Webcam webCam;

    public static void Finalizar() {
        FinalizarWebCam();
        if (getMainBusca() != null && getMainBusca().isAlive()) {
            getMainBusca().stop();
        }
        if (getOffBusca() != null && getOffBusca().isAlive()) {
            getOffBusca().stop();
        }
    }

    public static void FinalizarWebCam() {
        if (getTimerTaskWebCam() != null) {
            getTimerTaskWebCam().cancel();
        }
        if (getTimerWebCam() != null) {
            getTimerTaskWebCam().cancel();
        }
        if (getWebCam() != null) {
            getWebCam().close();
        }
    }

    /**
     * @return the timerWebCam
     */
    public static Timer getTimerWebCam() {
        return timerWebCam;
    }

    /**
     * @param aTimerWebCam the timerWebCam to set
     */
    public static void setTimerWebCam(Timer aTimerWebCam) {
        timerWebCam = aTimerWebCam;
    }

    /**
     * @return the timerTaskWebCam
     */
    public static TimerTask getTimerTaskWebCam() {
        return timerTaskWebCam;
    }

    /**
     * @param aTimerTaskWebCam the timerTaskWebCam to set
     */
    public static void setTimerTaskWebCam(TimerTask aTimerTaskWebCam) {
        timerTaskWebCam = aTimerTaskWebCam;
    }

    /**
     * @return the mainBusca
     */
    public static Thread getMainBusca() {
        return mainBusca;
    }

    /**
     * @param aMainBusca the mainBusca to set
     */
    public static void setMainBusca(Thread aMainBusca) {
        mainBusca = aMainBusca;
    }

    /**
     * @return the offBusca
     */
    public static Thread getOffBusca() {
        return offBusca;
    }

    /**
     * @param aOffBusca the offBusca to set
     */
    public static void setOffBusca(Thread aOffBusca) {
        offBusca = aOffBusca;
    }

    /**
     * @return the webCam
     */
    public static Webcam getWebCam() {
        return webCam;
    }

    /**
     * @param aWebCam the webCam to set
     */
    public static void setWebCam(Webcam aWebCam) {
        webCam = aWebCam;
    }

}
