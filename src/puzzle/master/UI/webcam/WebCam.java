/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puzzle.master.UI.webcam;

import com.github.sarxos.webcam.Webcam;
import java.awt.image.BufferedImage;
import java.util.Timer;
import java.util.TimerTask;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import puzzle.master.UI.celula.Celulas;
import puzzle.master.UI.celula.celula;
import puzzle.master.UI.threads.Threads;
import puzzle.util.Util;

/**
 *
 * @author Aluno
 */
public class WebCam {

    private Webcam webcam;

    public void init() throws Exception {

        webcam = Webcam.getDefault();
        webcam.open();
        Timer timer = new Timer();
        TimerTask t = new TimerTask() {
            private int factor;
            private Image imagem;

            @Override
            public void run() {
                BufferedImage image = webcam.getImage();
                factor = Util.getFactor(image);
                image = Util.resizeImage((java.awt.Image) image, factor, factor);
                imagem = SwingFXUtils.toFXImage(image, null);
                
                boolean flag = true;
                int i = 0, j = 0;
                int Pos = Celulas.getCelulaVazia().getPos();
                for (int PosN = 0; i < 3 && flag; i++) {
                    j = 0;
                    flag = PosN == Pos;
                    while (j < 3 && flag) {
                        j++;
                        PosN++;
                        flag = PosN == Pos;
                    }
                }
                Image[][] im = Util.processa_ImagemFaster_WebCam(imagem, i, j);
                Celulas.addImagems(im);
                Celulas.link();
            }
        };
        timer.scheduleAtFixedRate(t, 200, 18);
        t.run();
        Threads.setTimerWebCam(timer);
        Threads.setTimerTaskWebCam(t);
        Threads.setWebCam(webcam);

        //147
        //385
        //296
    }

    public Webcam getWebcam() {
        return webcam;
    }

    public void cancel() {
        Threads.FinalizarWebCam();
    }

}

/*// save image to PNG file
ImageIO.write(image, "PNG", new File("test.png"));*/
