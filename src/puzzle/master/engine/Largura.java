/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puzzle.master.engine;

import puzzle.master.engine.node.node;
import java.util.Arrays;
import puzzle.master.algorithms.Fila;

/**
 *
 * @author Aluno
 */
public class Largura extends Busca{

    public Largura()
    {
    }

    public Largura(Integer caso)
    {
        super(caso);
    }

    @Override
    protected node BuscaIterativa(node Resultado, node Problema) {
        caminho = new Fila();
        Fila oCaminho = (Fila) super.caminho;
        
        node atual = Problema;
        oCaminho.add(Problema);
        
        while (!resolvido(atual, Resultado) && !oCaminho.isEmpty()) {

            ///Retira da lista o proximo node
            atual = oCaminho.remove();
            ///Adiciona aos Percorridos
            percorrido.add(atual);
            
            ///Atualiza a Profundidade;
            int profundidade = atual.getProfundidade() + 1;
            if (profundidade < profundMax) {
                
                int[] posVazio = buscaEspacoVazio(atual.getEstado());
                int PosI = posVazio[0], PosJ = posVazio[1];
                
                if (PosI > 0) {
                    oCaminho.add(mover(atual, PosI, PosJ, PosI - 1, PosJ, profundidade, tipo));   // move o branco para cima
                }
                if (PosI < 2) {
                    oCaminho.add(mover(atual, PosI, PosJ, PosI + 1, PosJ, profundidade, tipo));   // move o branco para baixo
                }
                if (PosJ > 0) {
                    oCaminho.add(mover(atual, PosI, PosJ, PosI, PosJ - 1, profundidade, tipo));   // move o branco para a esquerda
                }
                if (PosJ < 2) {
                    oCaminho.add(mover(atual, PosI, PosJ, PosI, PosJ + 1, profundidade, tipo));   // move o branco para a direita
                }
                count++;
            }
        }

        if (resolvido(atual, Resultado)) {
            System.out.println(Arrays.deepToString(atual.getEstado()) + " " + atual.getProfundidade() + " " + count);
        } else {
            System.out.println("Sem Solução");
        }
        
        return atual;

    }

    @Override
    protected void setTipo() {
        tipo = "Largura";
    }
}