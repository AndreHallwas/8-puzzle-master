/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puzzle.master.engine;

import puzzle.master.engine.node.node;
import java.util.Arrays;
import puzzle.master.algorithms.Fila;
import puzzle.master.algorithms.Pilha;

/**
 *
 * @author Aluno
 */
public class Profundidade extends Busca {

    public Profundidade()
    {
    }

    public Profundidade(Integer caso)
    {
        super(caso);
    }

    @Override
    protected node BuscaIterativa(node Resultado, node Problema) {
        
        caminho = new Pilha();
        Pilha oCaminho = (Pilha) caminho;

        node atual = Problema;
        percorrido.push(Problema);
        oCaminho.push(Problema);

        while (!resolvido(atual, Resultado) && !oCaminho.isEmpty()) {

            ///Desempilha o proximo node
            atual = oCaminho.pop();
            ///Adiciona aos Percorridos
            percorrido.add(atual);

            ///Atualiza a Profundidade;
            int profundidade = atual.getProfundidade() + 1;
            if (profundidade < profundMax) {
                
                int[] posVazio = buscaEspacoVazio(atual.getEstado());
                int PosI = posVazio[0], PosJ = posVazio[1];
                
                if (PosI > 0) {
                    oCaminho.push(mover(atual, PosI, PosJ, PosI - 1, PosJ, profundidade, tipo));   // move o branco para cima
                }
                if (PosI < 2) {
                    oCaminho.push(mover(atual, PosI, PosJ, PosI + 1, PosJ, profundidade, tipo));   // move o branco para baixo
                }
                if (PosJ > 0) {
                    oCaminho.push(mover(atual, PosI, PosJ, PosI, PosJ - 1, profundidade, tipo));   // move o branco para a esquerda
                }
                if (PosJ < 2) {
                    oCaminho.push(mover(atual, PosI, PosJ, PosI, PosJ + 1, profundidade, tipo));   // move o branco para a direita
                }
                count++;
            }
        }

        if (resolvido(atual, Resultado)) {
            System.out.println(Arrays.deepToString(atual.getEstado()) + " " + atual.getProfundidade() + " " + count);
        } else {
            System.out.println("Sem Solução");
        }

        return atual;

    }

    @Deprecated
    protected node BuscaProfundidadeRecursiva(node Resultado, node atual) {
        ///Atualiza a Profundidade;
        int profundidade = atual.getProfundidade() + 1;
        node atualizado = atual;
        if (!resolvido(atual, Resultado)) {
            if (profundidade < profundMax) {
                int[][] estado = atual.getEstado();
                int PosI = -1, PosJ = -1;

                ///Adiciona aos Percorridos
                percorrido.add(atual);

                ///Encontra o espaço vazio;
                boolean flag = true;
                ///// +1 Corrige a saida de 3 para 2
                while (PosI + 1 < estado.length && flag) {
                    PosJ = -1;
                    PosI++;
                    while (PosJ + 1 < estado[PosI].length && flag) {
                        flag = estado[PosI][++PosJ] != 0;
                    }
                }
                if (PosI > 0 && !resolvido(atualizado, Resultado)) {
                    atualizado = mover(atual, PosI, PosJ, PosI - 1, PosJ, profundidade, tipo);   // move o branco para cima
                    atualizado = BuscaProfundidadeRecursiva(Resultado, atualizado);
                }
                if (PosI < 2 && !resolvido(atualizado, Resultado)) {
                    atualizado = mover(atual, PosI, PosJ, PosI + 1, PosJ, profundidade, tipo);   // move o branco para baixo
                    atualizado = BuscaProfundidadeRecursiva(Resultado, atualizado);
                }
                if (PosJ > 0 && !resolvido(atualizado, Resultado)) {
                    atualizado = mover(atual, PosI, PosJ, PosI, PosJ - 1, profundidade, tipo);   // move o branco para a esquerda
                    atualizado = BuscaProfundidadeRecursiva(Resultado, atualizado);
                }
                if (PosJ < 2 && !resolvido(atualizado, Resultado)) {
                    atualizado = mover(atual, PosI, PosJ, PosI, PosJ + 1, profundidade, tipo);   // move o branco para a direita
                    atualizado = BuscaProfundidadeRecursiva(Resultado, atualizado);
                }
                count++;
            }
        } else {
            System.out.println(Arrays.deepToString(atual.getEstado()) + " " + atual.getProfundidade() + " " + count);
        }
        return atualizado;
    }

    @Override
    protected void setTipo() {
        tipo = "Profundidade";
    }
}

/*

    public void BuscaProfundidadeRecursiva1(node Resultado, node atual) {
        ///Atualiza a Profundidade;
        int profundidade = atual.getProfundidade() + 1;

        if (!resolvido(atual, Resultado)) {
            if (profundidade < profundMax) {
                int[][] estado = atual.getEstado();
                int PosI = 0, PosJ = 0;

                ///Encontra o espaço vazio;
                boolean flag = true;
                ///// +1 Corrige a saida de 3 para 2
                while (PosI < estado.length && flag) {
                    PosJ = 0;
                    while (PosJ < estado[PosI].length && flag) {
                        if (estado[PosI][PosJ] == 0) {
                            node atualizado = null;
                            if (PosI > 0) {
                                atualizado = mover(atual, PosI, PosJ, PosI - 1, PosJ, profundidade, tipo);   // move o branco para cima
                                BuscaProfundidadeRecursiva1(Resultado, atualizado);
                            }
                            if (PosI < 2 && !resolvido(atualizado, Resultado)) {
                                atualizado = mover(atual, PosI, PosJ, PosI + 1, PosJ, profundidade, tipo);   // move o branco para baixo
                                BuscaProfundidadeRecursiva1(Resultado, atualizado);
                            }
                            if (PosJ > 0 && !resolvido(atualizado, Resultado)) {
                                atualizado = mover(atual, PosI, PosJ, PosI, PosJ - 1, profundidade, tipo);   // move o branco para a esquerda
                                BuscaProfundidadeRecursiva1(Resultado, atualizado);
                            }
                            if (PosJ < 2 && !resolvido(atualizado, Resultado)) {
                                atualizado = mover(atual, PosI, PosJ, PosI, PosJ + 1, profundidade, tipo);   // move o branco para a direita
                                BuscaProfundidadeRecursiva1(Resultado, atualizado);
                            }
                            flag = !flag;
                        }
                        PosJ++;
                    }
                    PosI++;
                }
            }
        } else {
            System.out.println(Arrays.deepToString(atual.getEstado()) + " " + atual.getProfundidade() + " " + count++);
        }
    }
 */
