/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puzzle.master.engine.node;

import java.util.Arrays;

/**
 *
 * @author Aluno
 */
public abstract class node {
    
    protected int[][] estado;
    protected int profundidade;
    protected node pai;

    public node(int[][] estado) {
        this.estado = estado;
        this.profundidade = 0;
        this.pai = null;
    }

    public node(int[][] estado, int profundidade, node pai) {
        this.estado = estado;
        this.profundidade = profundidade;
        this.pai = pai;
    }

    /**
     * @return the estado
     */
    public int[][] getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(int[][] estado) {
        this.estado = estado;
    }

    /**
     * @return the profundidade
     */
    public int getProfundidade() {
        return profundidade;
    }

    /**
     * @param profundidade the profundidade to set
     */
    public void setProfundidade(int profundidade) {
        this.profundidade = profundidade;
    }

    /**
     * @return the pai
     */
    public node getPai() {
        return pai;
    }

    /**
     * @param pai the pai to set
     */
    public void setPai(node pai) {
        this.pai = pai;
    }

    @Override
    public String toString() {
        //return "node{" + "estado=" + Arrays.deepToString(estado) + ", profundidade=" + profundidade + '}';
        return Arrays.deepToString(estado);
    }
    
}
