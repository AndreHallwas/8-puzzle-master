/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puzzle.master.engine.node;

/**
 *
 * @author Aluno
 */
public class nodeA extends node {

    private int valorf;
    private int valorg;
    private int valorh;
    
    public nodeA(int[][] estado) {
        super(estado);
        this.valorf = 0;
        this.valorg = 0;
        this.valorh = 0;
    }

    /**
     * @return the valorf
     */
    public int getValorf() {
        return valorf;
    }

    /**
     * @param valorf the valorf to set
     */
    public void setValorf(int valorf) {
        this.valorf = valorf;
    }

    /**
     * @return the valorg
     */
    public int getValorg() {
        return valorg;
    }

    /**
     * @param valorg the valorg to set
     */
    public void setValorg(int valorg) {
        this.valorg = valorg;
    }

    /**
     * @return the valorh
     */
    public int getValorh() {
        return valorh;
    }

    /**
     * @param valorh the valorh to set
     */
    public void setValorh(int valorh) {
        this.valorh = valorh;
    }
    
}
