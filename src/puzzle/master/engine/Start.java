/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puzzle.master.engine;

import puzzle.master.engine.node.node;

/**
 *
 * @author Aluno
 */
public class Start {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Busca b;
        /*node Problema = new node(new int[][]{{7, 3, 1}, {2, 0, 6}, {5, 4, 8}}) {
        };*/
        /*node Problema = new node(new int[][]{{4, 2, 3}, {6, 0, 1}, {7, 5, 8}}) {
        };*/
        /*node Problema = new node(new int[][]{{2, 3, 7}, {5, 4, 8}, {0, 6, 1}}) {
        };*/
        node Problema = new node(new int[][]{{1, 2, 3}, {4, 0, 5}, {7, 8, 6}}) {
        };
        node Solucao = new node(new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 0}}) {
        };
        b = new Profundidade();
        b.run(Solucao, Problema);
        b = new Largura();
        b.run(Solucao, Problema);
    }

}
