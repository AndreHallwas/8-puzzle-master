/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puzzle.master.engine;

import puzzle.master.util.Timer;
import puzzle.master.engine.node.node;
import java.util.Arrays;
import puzzle.master.algorithms.Fila;
import puzzle.master.algorithms.Pilha;

/**
 *
 * @author Raizen
 */
public abstract class Busca {

    /**
     * @return the caso
     */
    public Integer getCaso() {
        return caso;
    }

    /**
     * @param caso the caso to set
     */
    public void setCaso(Integer caso) {
        this.caso = caso;
    }
    private Integer caso;
    protected Object caminho;
    protected Pilha percorrido;
    protected node problema;
    protected node solucao;
    protected int profundMax = 28;
    protected int count;
    protected String tipo;
    protected Timer tempo;
    protected node Resolvido = null;
    private int ProfundFinal;
    private Fila caminhoSolucao;

    public Busca() {
    }

    public Busca(Integer caso) {
        this.caso = caso;
    }

    protected abstract void setTipo();

    protected abstract node BuscaIterativa(node Resultado, node Problema);

    public void run(node Resultado, node Problema) {
        percorrido = new Pilha();
        this.problema = Problema;
        this.solucao = Resultado;
        this.count = 0;
        this.tempo = new Timer();
        setTipo();
        Resolvido = BuscaIterativa(Resultado, Problema);
        this.setProfundFinal(Resolvido.getProfundidade());
        caminhoSolucao = geraCaminhoSolucao(Resolvido);
        System.out.println(tempo);
    }

    protected static node mover(node atual, int PosIAtual, int PosJAtual, int PosINova,
            int PosJNova, int Profundidade, String Tipo) {

        ///Faz a troca de elementos
        int[][] estado = copyMatriz(atual.getEstado());
        int aux = estado[PosIAtual][PosJAtual];
        estado[PosIAtual][PosJAtual] = estado[PosINova][PosJNova];
        estado[PosINova][PosJNova] = aux;

        ///Gera o node com o movimento realizado
        return new node(estado, Profundidade, atual) {
        };

    }

    public static int[][] mover(int[][] estadoAtual, int PosIAtual, int PosJAtual, int PosINova,
            int PosJNova) {

        ///Faz a troca de elementos
        int[][] estado = copyMatriz(estadoAtual);
        int aux = estado[PosIAtual][PosJAtual];
        estado[PosIAtual][PosJAtual] = estado[PosINova][PosJNova];
        estado[PosINova][PosJNova] = aux;

        return estado;

    }

    protected static int[][] copyMatriz(int[][] Matriz) {
        int[][] estado = new int[Matriz.length][];
        for (int i = 0; i < Matriz.length; i++) {
            estado[i] = new int[Matriz[i].length];
            for (int j = 0; j < Matriz[i].length; j++) {
                estado[i][j] = Matriz[i][j];
            }
        }
        return estado;
    }

    protected boolean resolvido(node atual, node estadoFinal) {
        return atual != null ? Arrays.deepEquals(atual.getEstado(), estadoFinal.getEstado()) : false;
    }

    protected Fila geraCaminhoSolucao(node atual) {
        Fila Caminho = new Fila();
        while (atual.getPai() != null) {
            Caminho.push(atual);
            System.out.println(atual);
            atual = atual.getPai();
        }
        return Caminho;
    }

    /**
     * @param estado
     * @return Array in [0] PosI, in [1] PosJ;
     */
    public static int[] buscaEspacoVazio(int[][] estado) {
        int PosI = -1, PosJ = -1;
        ///Encontra o espaço vazio;
        boolean flag = true;
        ///// +1 Corrige a saida de 3 para 2
        while (PosI + 1 < estado.length && flag) {
            PosJ = -1;
            PosI++;
            while (PosJ + 1 < estado[PosI].length && flag) {
                flag = estado[PosI][++PosJ] != 9;
            }
        }
        return new int[]{
            PosI, PosJ
        };
    }

    public boolean getResolvido() {
        return Resolvido != null;
    }

    /**
     * @return the caminhoSolucao
     */
    public Fila getCaminhoSolucao() {
        return caminhoSolucao;
    }

    /**
     * @return the caminho
     */
    public Object getCaminho() {
        return caminho;
    }

    /**
     * @param caminho the caminho to set
     */
    public void setCaminho(Object caminho) {
        this.caminho = caminho;
    }

    /**
     * @return the percorrido
     */
    public Pilha getPercorrido() {
        return percorrido;
    }

    /**
     * @param percorrido the percorrido to set
     */
    public void setPercorrido(Pilha percorrido) {
        this.percorrido = percorrido;
    }

    /**
     * @return the problema
     */
    public node getProblema() {
        return problema;
    }

    /**
     * @param problema the problema to set
     */
    public void setProblema(node problema) {
        this.problema = problema;
    }

    /**
     * @return the solucao
     */
    public node getSolucao() {
        return solucao;
    }

    /**
     * @param solucao the solucao to set
     */
    public void setSolucao(node solucao) {
        this.solucao = solucao;
    }

    /**
     * @return the profundMax
     */
    public int getProfundMax() {
        return profundMax;
    }

    /**
     * @param profundMax the profundMax to set
     */
    public void setProfundMax(int profundMax) {
        this.profundMax = profundMax;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the tempo
     */
    public Timer getTempo() {
        return tempo;
    }

    /**
     * @param tempo the tempo to set
     */
    public void setTempo(Timer tempo) {
        this.tempo = tempo;
    }

    /**
     * @return the ProfundFinal
     */
    public int getProfundFinal() {
        return ProfundFinal;
    }

    /**
     * @param ProfundFinal the ProfundFinal to set
     */
    public void setProfundFinal(int ProfundFinal) {
        this.ProfundFinal = ProfundFinal;
    }

}
