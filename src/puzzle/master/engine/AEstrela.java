/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puzzle.master.engine;

import java.util.Arrays;
import java.util.List;
import puzzle.master.algorithms.FilaPrioridade;
import puzzle.master.engine.node.node;
import puzzle.master.engine.node.nodeA;

/**
 *
 * @author Raizen
 */
public class AEstrela extends Busca
{

    private String modo = "H2";

    public AEstrela()
    {
    }

    public AEstrela(Integer caso)
    {
        super(caso);
    }

    @Override
    protected void setTipo()
    {
        tipo = "A*";
    }

    @Override
    protected node BuscaIterativa(node Resultado, node Problema)
    {
        nodeA atual = convert(Problema);

        atual.setValorh(calculaHeuristica(atual.getEstado(), modo));
        atual.setValorf(atual.getValorh());

        caminho = new FilaPrioridade();
        FilaPrioridade oCaminho = (FilaPrioridade) super.caminho;

        oCaminho.add(atual);

        while (!resolvido(atual, Resultado) && !oCaminho.isEmpty())
        {

            ///Retira da lista o proximo node
            atual = oCaminho.remove();

            ///Atualiza a Profundidade;
            int profundidade = atual.getProfundidade() + 1;
            if (profundidade < profundMax)
            {

                int[] posVazio = buscaEspacoVazio(atual.getEstado());
                int PosI = posVazio[0], PosJ = posVazio[1];

                if (PosI > 0)
                {
                    adiciona(oCaminho, atual, PosI, PosJ, PosI - 1, PosJ, profundidade, tipo);   // move o branco para cima
                }
                if (PosI < 2)
                {
                    adiciona(oCaminho, atual, PosI, PosJ, PosI + 1, PosJ, profundidade, tipo);   // move o branco para baixo
                }
                if (PosJ > 0)
                {
                    adiciona(oCaminho, atual, PosI, PosJ, PosI, PosJ - 1, profundidade, tipo);   // move o branco para a esquerda
                }
                if (PosJ < 2)
                {
                    adiciona(oCaminho, atual, PosI, PosJ, PosI, PosJ + 1, profundidade, tipo);   // move o branco para a direita
                }
                count++;

                ///Adiciona aos Percorridos
                percorrido.add(atual);
            }
        }

        if (resolvido(atual, Resultado))
        {
            System.out.println(Arrays.deepToString(atual.getEstado()) + " " + atual.getProfundidade() + " " + count);
        } else
        {
            System.out.println("Sem Solução");
        }

        return atual;

    }

    private void adiciona(FilaPrioridade oCaminho, nodeA atual, int PosI, int PosJ, int PosI1, int PosJ1, int profundidade, String tipo)
    {
        int valorg, valorf, valorh, i;
        if (procuraLista(percorrido, atual.getEstado()) != -1)
        {
            return;		// se j� est� na lista de nodos analisados, sai sem fazer nada
        }

        valorg = atual.getValorg() + 1;
        valorh = calculaHeuristica(atual.getEstado(), modo);
        valorf = valorg + valorh;

        nodeA filho = convert(mover(atual, PosI, PosJ, PosI1, PosJ1, profundidade, tipo));///
        filho.setValorf(valorf);
        filho.setValorg(valorg);
        filho.setValorh(valorh);

        i = procuraLista(oCaminho, atual.getEstado());	// se estado j� existe na lista de abertos, retorna indice do nodo correspondente
        if (i != -1)
        {
            if (oCaminho.get(i).getValorg() <= valorg) // custo do nodo na lista � menor/igual ao do gerado agora
            {
                return;	// sai sem colocar o filho gerado na lista
            } else
            {
                oCaminho.remove(i);	// remove o nodo antigo da lista
            }
        }
        oCaminho.inserir(filho); // adiciona o filho gerado A fila de prioridade

    }

    private int procuraLista(List oCaminho, int[][] estado)
    {
        int i = 0;
        while (i < oCaminho.size() && !comparaEstados(estado, ((nodeA) oCaminho.get(i)).getEstado()))
        {
            i++;
        }
        return i < oCaminho.size() ? i : -1;
    }

    private boolean comparaEstados(int[][] estado1, int[][] estado2)
    {
        int i = 0;
        boolean flag = true;
        while (i < estado1.length && flag)
        {
            for (int j = 0; j < estado1[i].length && flag; j++)
            {
                flag = estado1[i][j] == estado2[i][j];
            }
            i++;
        }
        return flag;
    }

    private int calculaHeuristica(int[][] estado, String modo)
    {
        int x, y, n = 0, d, py, px;
        if ("H1".equals(modo)) // Conta quantas peças estfo fora do lugar
        {
            for (y = 0; y < 3; y++)
            {
                for (x = 0; x < 3; x++)
                {
                    if (estado[y][x] != solucao.getEstado()[y][x] && estado[y][x] != 0)
                    {	// verifica peças fora do lugar, sem contar o espaço em branco
                        n++;
                    }
                }
            }
        } else if ("H2".equals(modo))
        {	// Calcula distancia total das peças de suas posições corretas
            for (y = 0; y < 3; y++)
            {
                for (x = 0; x < 3; x++)
                {
                    if (estado[y][x] != solucao.getEstado()[y][x] && estado[y][x] != 0)
                    {	// verifica peças fora do lugar, sem contar o espaço em branco
                        py = (estado[y][x] - 1) / 3;	// calcula linha e coluna correta
                        px = estado[y][x] - py * 3 - 1;
                        d = Math.abs(x - px) + Math.abs(y - py);// Manhattan Distance
                        n += d;
                    }
                }
            }
        }

        return n;
    }

    private nodeA convert(node node)
    {
        nodeA nodeN = new nodeA(node.getEstado());
        nodeN.setProfundidade(node.getProfundidade());
        nodeN.setPai(node.getPai());
        nodeN.setValorf(0);
        nodeN.setValorg(0);
        nodeN.setValorh(0);

        return nodeN;
    }
}
