/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puzzle.master.algorithms;

import java.util.LinkedList;
import puzzle.master.engine.node.nodeA;

/**
 *
 * @author Aluno
 */
public class FilaPrioridade extends LinkedList<nodeA> {

    public void inserir(nodeA node) {// insere na fila, ordenado pelo valor f do nodo

        int i = 0;

        while (i < size() && node.getValorf() > get(i).getValorf()) {
            i++;
        }
        if (i >= size()) {
            push(node);		// insere no fim
        } else {
            add(i, node);	// insere na posi��o i
        }
        
    }
}
