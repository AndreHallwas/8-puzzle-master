/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puzzle.util;

import java.awt.AlphaComposite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;

/**
 *
 * @author luis
 */
public class Util {

    @Deprecated
    public static Image[][] processa_Imagem(Image img) {
        int calcv = (int) (img.getHeight() / 3);
        int calch = (int) (img.getWidth() / 3);
        BufferedImage bi = SwingFXUtils.fromFXImage(img, null);
        BufferedImage[] bivet = new BufferedImage[9];
        for (int i = 0; i < bivet.length; i++) {
            bivet[i] = (i == 8)
                    ? new BufferedImage(calch, calcv, BufferedImage.TYPE_BYTE_GRAY)
                    : new BufferedImage(calch, calcv, BufferedImage.TYPE_4BYTE_ABGR);
        }
        for (int j = 0; j < calcv - 1; j++) {
            for (int k = 0; k < calch - 1; k++) {
                bivet[0].setRGB(j, k, bi.getRGB(j, k));
                bivet[1].setRGB(j, k, bi.getRGB(j + calch, k));
                bivet[2].setRGB(j, k, bi.getRGB(j + (calch * 2), k));

                bivet[3].setRGB(j, k, bi.getRGB(j, k + calcv));
                bivet[4].setRGB(j, k, bi.getRGB(j + calch, k + calcv));
                bivet[5].setRGB(j, k, bi.getRGB(j + calch * 2, k + calcv));

                bivet[6].setRGB(j, k, bi.getRGB(j, k + calcv * 2));
                bivet[7].setRGB(j, k, bi.getRGB(j + calch, k + calcv * 2));
                bivet[8].setRGB(j, k, bi.getRGB(j + calch * 2, k + calcv * 2));
            }
        }
        Image[][] im = new Image[3][3];
        int c = 0;
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                im[x][y] = SwingFXUtils.toFXImage(bivet[c++], null);
            }
        }
        return im;
    }

    public static Image[][] processa_ImagemFaster(Image img) {
        int calcv = (int) (img.getHeight() / 3);
        int calch = (int) (img.getWidth() / 3);
        Image[][] im = new Image[3][3];
        int c = calch * calcv;
        BufferedImage bi = SwingFXUtils.fromFXImage(img, null);
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                im[x][y] = SwingFXUtils.toFXImage(bi.getSubimage(y * calch, x * calcv, calch, calcv), null);
            }
        }
        im[2][2] = changeColorImage(im, 2, 2);
        return im;
    }

    public static Image[][] processa_ImagemFaster_WebCam(Image img, int PosX, int PosY) {
        int calcv = (int) (img.getHeight() / 3);
        int calch = (int) (img.getWidth() / 3);
        Image[][] im = new Image[3][3];
        int c = calch * calcv;
        BufferedImage bi = SwingFXUtils.fromFXImage(img, null);
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                im[x][y] = SwingFXUtils.toFXImage(bi.getSubimage(y * calch, x * calcv, calch, calcv), null);
            }
        }
        im[PosX][PosY] = changeColorImage(im, PosX, PosY);
        return im;
    }

    public static Image scale(Image source, int targetWidth, int targetHeight, boolean preserveRatio) {
        ImageView imageView = new ImageView(source);
        imageView.setPreserveRatio(preserveRatio);
        imageView.setFitWidth(targetWidth);
        imageView.setFitHeight(targetHeight);
        return imageView.snapshot(null, null);
    }

    /**
     * This function resize the image file and returns the BufferedImage object
     * that can be saved to file system.
     *
     * @param image
     * @param width
     * @param height
     * @return
     */
    public static BufferedImage resizeImage(java.awt.Image image, int width, int height) {
        final BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        final Graphics2D graphics2D = bufferedImage.createGraphics();
        graphics2D.setComposite(AlphaComposite.Src);
        //below three lines are for RenderingHints for better image quality at cost of higher processing time
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.drawImage(image, 0, 0, width, height, null);
        graphics2D.dispose();
        return bufferedImage;
    }

    /**
     * This function resize the image file and returns the BufferedImage object
     * that can be saved to file system.
     *
     * @param im
     * @param posX
     * @param posY
     * @return
     */
    public static WritableImage changeColorImage(Image[][] im, int posX, int posY) {
        BufferedImage image = new BufferedImage((int) im[posX][posY].getWidth(), (int) im[posX][posY].getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        Graphics g = image.getGraphics();
        g.drawImage(SwingFXUtils.fromFXImage(im[posX][posY], null), 0, 0, null);
        g.dispose();
        return SwingFXUtils.toFXImage(image, null);
    }

    public static int getFactor(BufferedImage bufferedImage) {
        int factor = 0;
        if (bufferedImage.getHeight() > bufferedImage.getWidth()) {
            factor = bufferedImage.getWidth();
        } else {
            factor = bufferedImage.getHeight();
        }
        return factor;
    }
}
