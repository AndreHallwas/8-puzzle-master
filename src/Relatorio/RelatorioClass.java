/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Relatorio;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import puzzle.master.engine.Busca;

/**
 *
 * @author luis
 */
public class RelatorioClass
{

    private String caso;
    private String ProfundFinal;
    private String solucao;
    private String problema;
    private String tempo;

    public RelatorioClass(Busca b)
    {
        this.caso = b.getCaso().toString();
        this.ProfundFinal = Integer.toString(b.getCount());
        this.solucao = b.getSolucao().toString();
        this.problema = b.getProblema().toString();
        this.tempo = b.getTempo().toString(true);
    }

    public RelatorioClass(String caso, String problema, String solucao, String tempo, String count)
    {
        this.caso = caso;
        this.ProfundFinal = count;
        this.solucao = solucao;
        this.problema = problema;
        this.tempo = tempo;
    }

    public static List<RelatorioClass> getRelatorioPreGerado(boolean flag, Object obj)
    {
        List<RelatorioClass> lr = new ArrayList<>();
        try
        {
            Reader r = new InputStreamReader(obj.getClass().getResourceAsStream((flag)? 
                    "/Relatorio/RelatorioPreGeradoNaoInformada.csv" :
                    "/Relatorio/RelatorioPreGeradoHeuristica.csv"));
            BufferedReader reader = new BufferedReader(r);
            List<String> lines = new ArrayList<>();
            String line = null;
            while ((line = reader.readLine()) != null)
            {
                lines.add(line.replace(" ", ""));
            }
            String []tline;
            for (int i = 0; i < lines.size(); i++)
            {
                tline = lines.get(i).split(";");
                lr.add(new RelatorioClass(tline[0], tline[1], tline[2], tline[3], tline[4]));
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return lr;
    }

    public String getCaso()
    {
        return caso;
    }

    public void setCaso(String caso)
    {
        this.caso = caso;
    }

    public String getSolucao()
    {
        return solucao;
    }

    public void setSolucao(String solucao)
    {
        this.solucao = solucao;
    }

    public String getProblema()
    {
        return problema;
    }

    public void setProblema(String problema)
    {
        this.problema = problema;
    }

    public String getTempo()
    {
        return tempo;
    }

    public void setTempo(String tempo)
    {
        this.tempo = tempo;
    }

    /**
     * @return the ProfundFinal
     */
    public String getProfundFinal() {
        return ProfundFinal;
    }

    /**
     * @param ProfundFinal the ProfundFinal to set
     */
    public void setProfundFinal(String ProfundFinal) {
        this.ProfundFinal = ProfundFinal;
    }
    
    
}
