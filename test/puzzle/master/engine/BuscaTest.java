/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puzzle.master.engine;

import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import puzzle.master.engine.node.node;

/**
 *
 * @author Aluno
 */
public class BuscaTest {

    protected List<node> Problemas;
    protected node Solucao;

    public BuscaTest() {
        Problemas = new ArrayList();

        Problemas.add(new node(new int[][]{{7, 3, 1}, {2, 0, 6}, {5, 4, 8}}) {
        });
        /*Problemas.add(new node(new int[][]{{4, 2, 3}, {6, 0, 1}, {7, 5, 8}}) {
        });*/
        /*Problemas.add(new node(new int[][]{{2, 3, 7}, {5, 4, 8}, {0, 6, 1}}) {
        });*/
        /*Problemas.add(new node(new int[][]{{1, 2, 3}, {4, 0, 5}, {7, 8, 6}}) {
        });*/

        Solucao = new node(new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 0}}) {
        };
    }

    /**
     * Teste da busca em largura
     */
    @Test
    public void testLargura() {
        Busca b;
        for (node Problema : Problemas) {
            b = new Largura();
            b.run(Solucao, Problema);
            Assert.assertEquals(true, b.getResolvido());
        }
    }

    /**
     * teste da busca em profundidade
     */
    @Test
    public void testProfundidade() {
        Busca b;
        for (node Problema : Problemas) {
            b = new Profundidade();
            b.run(Solucao, Problema);
            Assert.assertEquals(true, b.getResolvido());
        }
    }

}
